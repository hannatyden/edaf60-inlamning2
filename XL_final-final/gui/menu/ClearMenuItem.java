package gui.menu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JMenuItem;
import gui.CurrentSlot;
import model.SpreadSheet;

class ClearMenuItem extends JMenuItem implements ActionListener {
  private CurrentSlot currentSlot;
  private SpreadSheet sheet;

  public ClearMenuItem(CurrentSlot currentSlot, SpreadSheet sheet) {
    super("Clear");
    this.currentSlot = currentSlot;
    this.sheet = sheet;
    addActionListener(this);
  }

  public void actionPerformed(ActionEvent e) {

    try {
      sheet.clearSlot(currentSlot.getAddress());
    } catch (Exception e1) {

    }

  }
}
