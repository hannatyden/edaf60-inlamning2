package gui.menu;

import gui.StatusLabel;
import java.util.Map;
import gui.XL;
import model.Slot;
import model.SpreadSheet;

import java.io.FileNotFoundException;
import java.util.Set;

import javax.swing.JFileChooser;

class SaveMenuItem extends OpenMenuItem {

  private SpreadSheet sheet;
  private XLPrintStream ps;

  public SaveMenuItem(XL xl, StatusLabel statusLabel, SpreadSheet sheet) {
    super(xl, statusLabel, "Save");
    this.sheet = sheet;
  }

  protected void action(String path) throws FileNotFoundException {
    ps = new XLPrintStream(path);
    Set<Map.Entry<String, Slot>> map = sheet.getSlotMap().entrySet();
    ps.save(map);
  }

  protected int openDialog(JFileChooser fileChooser) {
    return fileChooser.showSaveDialog(xl);
  }
}
