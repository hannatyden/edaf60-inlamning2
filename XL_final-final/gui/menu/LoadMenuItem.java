package gui.menu;

import gui.StatusLabel;
import gui.XL;
import model.Slot;
import model.SpreadSheet;

import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JFileChooser;

class LoadMenuItem extends OpenMenuItem {
  private SpreadSheet sheet;

  public LoadMenuItem(XL xl, StatusLabel statusLabel, SpreadSheet sheet) {
    super(xl, statusLabel, "Load");
    this.sheet = sheet;
  }

  protected void action(String path) throws FileNotFoundException {
    statusLabel.clear();

    try {
      XLBufferedReader buffer = new XLBufferedReader(path);
      System.out.println("loaded file1");
      Map<String, String> slotMap = new HashMap<String, String>();
      System.out.println("loaded file2");
      buffer.load(slotMap);
      System.out.println("loaded file3");
      sheet.clearAll();
      System.out.println("loaded file4");
      sheet.load(slotMap);

    } catch (Exception ex) {
      statusLabel.setText("Error: " + ex.getMessage());
    }
  }

  protected int openDialog(JFileChooser fileChooser) {
    return fileChooser.showOpenDialog(xl);
  }
}
