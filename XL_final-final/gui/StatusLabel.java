package gui;

import java.awt.Color;
import java.util.Observable;
import java.util.Observer;

import model.SpreadSheet;

public class StatusLabel extends ColoredLabel implements Observer {
  private SpreadSheet sheet;

  public StatusLabel() {
    super("", Color.WHITE);
    sheet = new SpreadSheet();
    sheet.addObserver(this);
  }

  public void update(Observable observable, Object object) {
    setText("");
  }

  public void clear() {

    setText("");
  }
}
