package gui;

import java.awt.Color;
import java.util.Observable;

import model.Slot;

public class CurrentSlot extends Observable {
  private String address;


  public void set(String address) {
    this.address = address;
    setChanged();
    notifyObservers();
    System.out.println("current slot changed: " + address);
  }

  public String getAddress() {
    return address;
  }

}
