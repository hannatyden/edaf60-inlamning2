package gui;

import java.awt.Color;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Observable;
import java.util.Observer;

import model.SpreadSheet;

public class SlotLabel extends ColoredLabel implements Observer, MouseListener {
  private String name;
  private CurrentSlot currentSlot;
  private SpreadSheet sheet;
  private StatusLabel sLabel;
  private SlotLabels slotLabels;

  public SlotLabel(String name, CurrentSlot currentSlot, SpreadSheet sheet, StatusLabel sLabel,
      SlotLabels slotLabels) {
    super("                    ", Color.WHITE, RIGHT);
    addMouseListener(this);
    this.name = name;
    this.currentSlot = currentSlot;
    this.sheet = sheet;
    this.sLabel = sLabel;
    this.slotLabels = slotLabels;
  }

  @Override
  public void update(Observable o, Object arg) {
    updateText();
  }

  public void updateText() {
    setText(sheet.getValue(currentSlot.getAddress()));
  }



  public String getName() {
    return name;
  }

  @Override
  public void mouseClicked(MouseEvent e) {
    slotLabels.setWhite(currentSlot.getAddress());
    setBackground(Color.YELLOW);
    currentSlot.set(name);
    if (sLabel != null)
      sLabel.clear();
    System.out.println(currentSlot.getAddress());


  }

  @Override
  public void mousePressed(MouseEvent e) {
    // TODO Auto-generated method stub

  }

  @Override
  public void mouseReleased(MouseEvent e) {
    // TODO Auto-generated method stub

  }

  @Override
  public void mouseEntered(MouseEvent e) {
    // TODO Auto-generated method stub

  }

  @Override
  public void mouseExited(MouseEvent e) {
    // TODO Auto-generated method stub

  }


}
