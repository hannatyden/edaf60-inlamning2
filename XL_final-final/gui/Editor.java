package gui;

import java.awt.Color;
import javax.swing.JTextField;
import expr.Expr;
import expr.ExprParser;
import model.ExprSlot;
import model.SpreadSheet;
import util.XLException;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.Observable;
import java.util.Observer;


public class Editor extends JTextField implements ActionListener, Observer {

  private CurrentSlot currentSlot;
  private SpreadSheet sheet;
  private StatusLabel sLabel;

  public Editor(CurrentSlot currentSlot, SpreadSheet sheet, StatusLabel sLabel) {
    this.currentSlot = currentSlot;
    this.sheet = sheet;
    this.sLabel = sLabel;
    setBackground(Color.WHITE);
    addActionListener(this);
    currentSlot.addObserver(this);

  }

  @Override
  public void actionPerformed(ActionEvent a) {
    String value = getText();
    String address = currentSlot.getAddress();
    String temp = sheet.getValue(address);
    try {

      if (value.equals("")) {
        sheet.put(address, "");
      }
      sheet.put(address, value);
      sLabel.setText("");
    } catch (Exception e) {
      try {
        sheet.put(address, temp);
      } catch (Exception e1) {

      }
      sLabel.setText(e.getMessage());
    }
  }

  @Override
  public void update(Observable obv, Object obj) {
    setText(sheet.getValue(currentSlot.getAddress()));
  }
}
