package gui;

import static java.awt.BorderLayout.CENTER;
import static java.awt.BorderLayout.WEST;

import model.SpreadSheet;

public class SheetPanel extends BorderPanel {
  private SlotLabels sLabels;

  public SheetPanel(int rows, int columns, CurrentSlot currentSlot, SpreadSheet sheet,
      StatusLabel sLabel) {
    add(WEST, new RowLabels(rows));
    sLabels = new SlotLabels(rows, columns, currentSlot, sheet, sLabel);
    add(CENTER, sLabels);

  }
}
