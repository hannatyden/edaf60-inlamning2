package gui;

import static java.awt.BorderLayout.CENTER;
import static java.awt.BorderLayout.WEST;

public class StatusPanel extends BorderPanel {
  CurrentLabel currentLabel;

  protected StatusPanel(StatusLabel statusLabel, CurrentSlot currentSlot) {
    this.currentLabel = new CurrentLabel(currentSlot);
    add(WEST, currentLabel);
    add(CENTER, statusLabel);
  }
}
