package model;

import expr.Environment;
import util.XLException;

public class ErrorSlot implements Slot {
  private String value;

  public ErrorSlot(String value) {
    this.value = value;
  }

  @Override
  public String display(Environment env) {
    return "";
  }

  @Override
  public double getValue(Environment env) {
    return 0;
  }

  @Override
  public String toString() {
    return "";
  }



}
