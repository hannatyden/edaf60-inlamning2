package model;

import expr.Environment;
import expr.Expr;

public class ExprSlot implements Slot {
  private Expr expr;

  public ExprSlot(Expr expr) {
    this.expr = expr;
  }

  public String display(Environment environment) {
    return String.valueOf(expr.value(environment));
  }

  @Override
  public double getValue(Environment environment) {
    return expr.value(environment);
  }


  @Override
  public String toString() {
    return expr.toString();
  }
}
