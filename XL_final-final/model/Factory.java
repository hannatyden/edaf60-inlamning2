package model;

import expr.ExprParser;
import util.XLException;

public class Factory {

  public Slot createSlot(String string) throws Exception {
    ExprParser parser = new ExprParser();
    if (string.length() > 0) {
      if (string.charAt(0) == '#') {
        return new CommentSlot(string);
      } else {
        try {
          return new ExprSlot(parser.build(string));
        } catch (Exception e) {
          throw e;
        }
      }
    }
    return new EmptySlot("0");

  }
}
