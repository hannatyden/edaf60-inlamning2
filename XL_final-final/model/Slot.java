package model;

import expr.Environment;

public interface Slot {

  public double getValue(Environment env);

  public String toString();

  public String display(Environment env);

}
