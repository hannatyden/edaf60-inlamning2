package model;

import java.util.HashMap;
import java.util.Map;
import java.util.Observable;
import expr.Environment;
import gui.SlotLabels;
import util.XLException;

public class SpreadSheet extends Observable implements Environment {
  private Map<String, Slot> slotMap;
  private Factory factory;


  public SpreadSheet() {
    slotMap = new HashMap<String, Slot>();
    factory = new Factory();
  }

  public void put(String key, String value) throws Exception {

    try {
      slotMap.put(key, factory.createSlot(value));
      update();
    } catch (Exception e) {
      throw e;
    }


  }

  public String displayValue(String address) {
    return slotMap.get(address).display(this);
  }


  public Slot getSlot(String addr) {
    return slotMap.get(addr);
  }

  public Map<String, Slot> getSlotMap() {
    return slotMap;
  }

  public String getValue(String key) {
    return slotMap.get(key).toString();
  }


  public void clearSlot(String s) throws Exception {

    put(s, "");
    update();


  }

  public void clearAll() {
    slotMap.replaceAll((k, v) -> new EmptySlot(""));
    update();
  }

  public Factory getFactory() {
    return this.factory;
  }


  public void update() {
    setChanged();
    notifyObservers();
  }

  @Override
  public double value(String name) {
    Slot value = slotMap.get(name);
    if (value == null)
      throw new XLException("Empty Slot: " + name);
    return value.getValue(this);

  }

  public void load(Map<String, String> newSlotMap) throws Exception {
    for (String s : newSlotMap.keySet()) {
      try {
        put(s, newSlotMap.get(s));
      } catch (Exception e) {
        throw e;
      }
    }
  }



  public void print() {
    for (String k : slotMap.keySet()) {
      String v = getValue(k);
      System.out.println(k + "->" + v);
    }
  }

  public void put(String key, Slot slot) {
    slotMap.put(key, slot);

  }


}
