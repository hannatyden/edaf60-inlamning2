package gui;

import java.awt.Color;
import javax.swing.JTextField;
import expr.Expr;
import expr.ExprParser;
import model.ExprSlot;
import model.SpreadSheet;
import util.XLException;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.Observable;
import java.util.Observer;


public class Editor extends JTextField implements ActionListener, Observer {

  private CurrentSlot currentSlot;
  private SpreadSheet sheet;
  private StatusLabel sLabel;

  public Editor(CurrentSlot currentSlot, SpreadSheet sheet, StatusLabel sLabel) {
    this.currentSlot = currentSlot;
    this.sheet = sheet;
    this.sLabel = sLabel;
    setBackground(Color.WHITE);
    addActionListener(this);
    currentSlot.addObserver(this);

  }

  @Override
  public void actionPerformed(ActionEvent a) {
    sLabel.clear();
    String name = currentSlot.getAddress();
    String prevValue = sheet.getValue(name);
    if (getText().equals("")) {
      sheet.clearSlot(name);
      setText("");
    } else if (getText().charAt(0) == '#') {

      try {
        sheet.put(name, getText());
        sheet.updateAll();
      } catch (XLException | IOException e) {
        sLabel.setText(e.getMessage());
      }

    } else


    if (getText().charAt(0) != '#') {
      try {
        Expr expr = new ExprParser().build(getText());
        sheet.put(name, new ExprSlot(expr));

      } catch (XLException | IOException e) {
        sLabel.setText(e.getMessage());
      }

      try {
        sheet.updateAll();
      } catch (XLException e) {
        sLabel.setText(" " + e.getMessage());
        try {
          sheet.put(name, prevValue);
        } catch (IOException e1) {
          sLabel.setText(" " + e.getMessage());
        }
      }
    }
  }

  @Override
  public void update(Observable obv, Object obj) {
    setText(sheet.getValue(currentSlot.getAddress()));
  }
}
