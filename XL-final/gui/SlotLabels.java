package gui;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import javax.swing.SwingConstants;

import expr.Environment;
import model.SpreadSheet;

public class SlotLabels extends GridPanel {
  private List<SlotLabel> labelList;
  private CurrentSlot currentSlot;
  private SpreadSheet sheet;
  private StatusLabel sLabel;

  public SlotLabels(int rows, int cols, CurrentSlot currentSlot, SpreadSheet sheet,
      StatusLabel sLabel) {
    super(rows + 1, cols);
    this.currentSlot = currentSlot;
    this.sheet = sheet;
    this.sLabel = sLabel;
    labelList = new ArrayList<SlotLabel>(rows * cols);
    for (char ch = 'A'; ch < 'A' + cols; ch++) {
      add(new ColoredLabel(Character.toString(ch), Color.LIGHT_GRAY, SwingConstants.CENTER));
    }
    for (int row = 1; row <= rows; row++) {
      for (char ch = 'A'; ch < 'A' + cols; ch++) {
        String name = ch + String.valueOf(row);
        SlotLabel label = new SlotLabel(name, currentSlot, sheet, sLabel);
        add(label);
        labelList.add(label);
      }
    }
    SlotLabel first = labelList.get(0);
    first.setBackground(Color.YELLOW);
    currentSlot.set(first.getName());
  }

  public void clearAll() {
    for (SlotLabel sl : labelList) {
      sl.setText("");
    }
  }

  public void setWhite(String label) {
    for (SlotLabel sl : labelList) {
      if (sl.getName() == label) {
        sl.setBackground(Color.WHITE);
      }
    }
  }

  public List<SlotLabel> getList() {
    return labelList;
  }


  public void updateAll(Environment environment) {
    for (SlotLabel sl : labelList) {
      sl.setText(sheet.getSlot(sl.getName()).display(environment));
    }
  }

  public void updateCurrent(Environment environment) {
    for (SlotLabel sl : labelList) {
      System.out.println(sl.getName());
      if (sl.getName() == currentSlot.getAddress()) {
        sl.setText(sheet.getSlot(currentSlot.getAddress()).display(environment));
      }
    }
  }
}
