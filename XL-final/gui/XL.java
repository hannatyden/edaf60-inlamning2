package gui;

import model.ErrorSlot;
import model.SpreadSheet;
import static java.awt.BorderLayout.CENTER;
import static java.awt.BorderLayout.NORTH;
import static java.awt.BorderLayout.SOUTH;
import gui.menu.XLMenuBar;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.io.IOException;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class XL extends JFrame implements Printable {
  private static final int ROWS = 10, COLUMNS = 8;
  private XLCounter counter;
  private StatusLabel sLabel = new StatusLabel();
  private XLList xlList;

  public XL(XL oldXL) {
    this(oldXL.xlList, oldXL.counter);
  }

  public XL(XLList xlList, XLCounter counter) {
    super("Untitled-" + counter);
    this.xlList = xlList;
    this.counter = counter;
    CurrentSlot currentSlot = new CurrentSlot();
    SpreadSheet sheet = new SpreadSheet();
    for (char ch = 'A'; ch < 'A' + COLUMNS + 1; ch++) {
      for (int i = 0; i < ROWS + 1; i++) {
        try {
          sheet.put("" + ch + i, "");
        } catch (IOException e) {

          e.printStackTrace();
        }
      }
    }
    xlList.add(this);
    counter.increment();
    JPanel statusPanel = new StatusPanel(sLabel, currentSlot);
    JPanel sheetPanel = new SheetPanel(ROWS, COLUMNS, currentSlot, sheet, sLabel);
    Editor editor = new Editor(currentSlot, sheet, sLabel);
    add(NORTH, statusPanel);
    add(CENTER, editor);
    add(SOUTH, sheetPanel);
    setJMenuBar(new XLMenuBar(this, xlList, sLabel, currentSlot, sheet));
    pack();
    setDefaultCloseOperation(EXIT_ON_CLOSE);
    setResizable(false);
    setVisible(true);
  }

  public int print(Graphics g, PageFormat pageFormat, int page) {
    if (page > 0)
      return NO_SUCH_PAGE;
    Graphics2D g2d = (Graphics2D) g;
    g2d.translate(pageFormat.getImageableX(), pageFormat.getImageableY());
    printAll(g2d);
    return PAGE_EXISTS;
  }

  public void rename(String title) {
    setTitle(title);
    xlList.setChanged();
  }

  public static void main(String[] args) {
    new XL(new XLList(), new XLCounter());
  }
}
