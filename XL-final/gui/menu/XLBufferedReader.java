package gui.menu;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Map;

import model.Factory;
import model.Slot;
import util.XLException;

// TODO move to another package
public class XLBufferedReader extends BufferedReader {
  private Factory factory;

  public XLBufferedReader(String name) throws FileNotFoundException {
    super(new FileReader(name));
  }

  // TODO Change Object to something appropriate
  public void load(Map<String, String> map) {
    try {
      while (ready()) {
        System.out.println("loaded file5");
        String string = readLine();
        System.out.println("loaded file6");
        int i = string.indexOf('=');
        System.out.println("loaded file7");
        // TODO
        String var = string.substring(0, i);
        System.out.println("loaded file8");
        String val = string.substring(i + 1);
        System.out.println("loaded file9");
        if (val.length() > 0) {
          System.out.println("loaded file10");
          map.put(var, val);
          System.out.println("loaded file11");
        }
      }
    } catch (Exception e) {
      throw new XLException(e.getMessage());
    }
  }
}
