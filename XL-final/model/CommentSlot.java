package model;

import expr.Environment;

public class CommentSlot implements Slot {
  private String comment;

  public CommentSlot(String comment) {
    this.comment = comment;
  }

  public String display(Environment environment) {
    return comment.substring(1);
  }

  @Override
  public String toString() {
    return comment;

  }

  @Override
  public double getValue(Environment environment) {
    // TODO Auto-generated method stub
    return 0;
  }
}
