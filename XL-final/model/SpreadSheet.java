package model;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Observable;
import expr.Environment;
import expr.Expr;
import gui.SlotLabel;
import gui.SlotLabels;
import util.XLException;

public class SpreadSheet extends Observable implements Environment {
  private Map<String, Slot> slotMap;
  private Factory factory;
  public static Slot currentSlot;
  private SlotLabels slotLabels;

  public SpreadSheet() {
    slotMap = new HashMap<String, Slot>();
    factory = new Factory();
  }

  public void put(String key, String value) throws IOException {
    slotMap.put(key, factory.createSlot(value));
  }


  public Slot getSlot(String addr) {
    return slotMap.get(addr);
  }

  public Map<String, Slot> getSlotMap() {
    return slotMap;
  }

  public String getValue(String key) {
    return slotMap.get(key).toString();
  }

  
  public void clearSlot(String s) {
    slotMap.put(s, new EmptySlot("0"));
    updateAll();
  }

  public void clearAll() {
    slotMap.replaceAll((k, v) -> new EmptySlot(""));
    slotLabels.clearAll();
    update();
  }

  public Factory getFactory() {
    return this.factory;
  }

  public SlotLabels getSlotLabels() {
    return slotLabels;
  }

  public void update() {
    setChanged();
    notifyObservers();
  }

  @Override
  public double value(String name) {
    Slot value = slotMap.get(name);
    if (value == null)
      throw new XLException("Empty Slot: " + name);
    return value.getValue(this);

  }

  public void load(Map<String, String> newSlotMap) {
    for (String s : newSlotMap.keySet()) {
      try {
        put(s, newSlotMap.get(s));
      } catch (IOException e) {
        System.out.println("hejdååå");
        e.printStackTrace();
      }
    }
  }

  public void putAll(SlotLabels sLabels) {
    this.slotLabels = sLabels;
  }

  public void updateCurrent() {
    slotLabels.updateCurrent(this);
  }

  public void updateAll() {
    slotLabels.updateAll(this);
  }

  public void print() {
    for (String k : slotMap.keySet()) {
      String v = getValue(k);
      System.out.println(k + "->" + v);
    }
  }

  public void put(String key, Slot slot) {
    slotMap.put(key, slot);

  }


}
