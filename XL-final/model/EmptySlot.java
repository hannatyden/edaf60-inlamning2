package model;

import expr.Environment;

public class EmptySlot implements Slot {
  private String s;

  public EmptySlot(String s) {
    this.s = s;
  }


  @Override
  public double getValue(Environment env) {
    return 0;
  }

  @Override
  public String display(Environment env) {
    return "";
  }

  @Override
  public String toString() {
    return "";
  }

}
