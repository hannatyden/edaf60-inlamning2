package model;

import java.io.IOException;

import expr.Expr;
import expr.ExprParser;
import util.XLException;
import java.io.IOException;

public class Factory {

  public Slot createSlot(String string) throws IOException {
    ExprParser parser = new ExprParser();
    if (string.length() > 0) {
      if (string.charAt(0) == '#') {
        return new CommentSlot(string);
      } else {
        try {
          return new ExprSlot(parser.build(string));
        } catch (XLException e) {
          return new ErrorSlot("0");
        }
      }
    }
    return new EmptySlot("0");

  }
}
