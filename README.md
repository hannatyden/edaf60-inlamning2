# EDAF60-inlamning2

### Användningsfall 1: 
Användaren vill addera A1 med 2. (Vi förutsätter att A1 är antingen tom eller innehåller en siffra). Användaren går till valfri ruta och matar in A1+2 → resultatet läggs i den rutan användaren är i som "2.0". 

### Användningsfall 2:
Användaren vill skriva en kommentar i ruta b3.
Användaren klickar med hjälp av musen på ruta B3 och skriver in #hej. Därefter klickar användaren enter och kommentaren sparas i slotet B3 som "hej".

### Användningsfall 3:
Användaren vill ladda upp en fil kallad “fibonacci”. Användaren trycker på “load” och väljer filen “fibonacci” och trycker därefter öppna. Eftersom denna fil fungerar så laddas soffror in i sheet:et. 

### Användningsfall 4:
Användaren vill addera B1 + #hej. (Vi förutsätter att B1 är antingen tom eller innehåller en siffra). Användaren går till valfri ruta och matar in B1+hej → användaren får felmeddelandet “unexpected #”
